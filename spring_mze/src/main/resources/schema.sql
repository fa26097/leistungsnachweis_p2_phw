/* Initialer Test der H2-Datenbank, Tabellen werden über Entities erzeugt, wird nicht mehr benötigt
DROP TABLE IF EXISTS mitarbeiter;
DROP TABLE IF EXISTS zeiterfassung;

CREATE TABLE mitarbeiter(
    persnr int PRIMARY KEY,
    name varchar(255) NOT NULL,
    vorname varchar(255) NOT NULL,
    gbdatum date NOT NULL
);

CREATE TABLE zeiterfassung(
    zeid int PRIMARY KEY AUTO_INCREMENT,
    datum date NOT NULL,
    von time NOT NULL,
    bis time NOT NULL,
    taetigkeit varchar(255) NOT NULL,
    persnr int NOT NULL,
    FOREIGN KEY (persnr) REFERENCES mitarbeiter(persnr)
);

INSERT INTO mitarbeiter(persnr,name,vorname,gbdatum) values (2000,'Weber','Philipp','1997-04-13');
INSERT INTO mitarbeiter(persnr,name,vorname,gbdatum) values (2400,'Hans','Peter','1975-01-28');
*/
