package dev.mze.spring.controller;

import dev.mze.spring.model.mitarbeiter.Mitarbeiter;
import dev.mze.spring.model.zeiterfassung.Zeiterfassung;
import dev.mze.spring.model.zeiterfassung.ZeiterfassungDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class ZeiterfassungController {

    @Autowired
    private ZeiterfassungDAO zeiterfassungDAO;

    @GetMapping("/zeiterfassung/get-all")
    public List<Zeiterfassung> getallZeiterfassung() {
        return zeiterfassungDAO.getallZeiterfassung();
    }

    @GetMapping("/zeiterfassung/get-single/{id}")
    public ResponseEntity<Zeiterfassung> getsingleZeiterfassung(@PathVariable(value="id") Integer zeid) {
        Optional<Zeiterfassung> z = zeiterfassungDAO.getsingleZeiterfassung(zeid);
        if (z.isPresent()) {
            return ResponseEntity.ok(z.get());
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping("/zeiterfassung/save")
    public Zeiterfassung save(@RequestBody Zeiterfassung zeiterfassung) {
        return zeiterfassungDAO.save(zeiterfassung);
    }
}
