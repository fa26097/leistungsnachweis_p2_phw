package dev.mze.spring.model.mitarbeiter;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MitarbeiterRepository extends CrudRepository<Mitarbeiter, Integer> {

}
