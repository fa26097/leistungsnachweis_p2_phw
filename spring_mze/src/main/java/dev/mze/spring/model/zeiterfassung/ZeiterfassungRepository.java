package dev.mze.spring.model.zeiterfassung;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ZeiterfassungRepository extends CrudRepository<Zeiterfassung, Integer> {

}
